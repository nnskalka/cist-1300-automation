// html5 syntax as defined here:
// https://www.w3schools.com/html/html5_syntax.asp
const lintOpts = {
    // Use Correct Document Type
    "doctype-first" : "smart",
    // Use Lower Case Element Names
    "tag-name-lowercase" : true,
    "tag-name-match" : true,
    // Close All HTML Elements
    "tag-close" : true,
    // Use Lower Case Attribute Names
    "attr-name-style" : "dash",
    // Quote Attribute Values
    "attr-quote-style" : "double",
    // Image Attributes (Always define alt, height, and width)
    "img-req-alt" : true,

    // Never omitting <html> and <body>
    "html-valid-content-model" : true,
    "html-req-lang" : true,
    "lang-style" : "case",
    // Meta Data
    "head-req-title" : true,
    // Tables
    "table-req-header" : true,

    // Common file things
    "line-no-trailing-whitespace" : true,
    "line-end-style" : false,
    "indent-style" : false,
    "indent-width" : false,
    "indent-delta" : false,
    "attr-bans" : false,
    "tag-bans" : false
};

String.prototype.format = function() {
    var a = this;

    for (const k in arguments) {
        while (a.includes("{" + k + "}"))
            a = a.replace("{" + k + "}", arguments[k]);
    }

    return a
};

const fs = require('fs');

let walk = function(dir) {
    let results = [];
    const list = fs.readdirSync(dir);

    list.forEach(function(file) {
        file = dir + '/' + file;
        var stat = fs.statSync(file);
        if (stat && stat.isDirectory()) {
            results = results.concat(walk(file));
        } else {
            results.push(file);
        }
    });

    return results;
};

const htmllint = require('htmllint');
let link = "https://github.com/htmllint/htmllint/wiki/Options";
var didEncounterIssue = false;
let promises = [];
walk('cist-1300').forEach(function(path) {
    if (path.includes(".html")) {
        const file = fs.readFileSync(path, 'utf8');

        var thing = Promise.resolve(htmllint(file, lintOpts));
        promises.push(thing);
        thing.then(function(output) {
            output.forEach(function(issue) {
                didEncounterIssue = true;
                var issueName = (issue.rule !== null) ? issue.rule : issue.data.option;
                var issueMessage = "Error - \"{0}\" ({1}, {2}): {3} (Code: {4}) ({5}#{3})"
                    .format(path, issue.line, issue.column, issueName, issue.code, link);
                console.log(issueMessage);
            });
        });
    }
});

Promise.all(promises).then(function() {
    if (didEncounterIssue) {
        console.log("If an issue link is broken: Seach by Code at https://github.com/htmllint/htmllint/wiki/Option-by-Error-Code");
    }
});