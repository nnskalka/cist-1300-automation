# CIST-1300-Automation

This is my project folder for the UNO CIST1300 course. I have added few things to the project: gradle.

### How to Use

1. Install Java (see: 'Installing Java' below)

2. Download this repo. 
    * There should be a small dropdown to the top right of the repo file directory that looks like a download cloud. Click to download (as zip), then extract the files. Move only the files that look like this repo directory to your personal repo folder (Ex: CIST-1300-Automation/{the real files}). Take the real files and move them into your local class repo, then move your labs into the cist-1300 folder.

3. Run the relevant Gradle publish/lint command listed below

### Gradle
Gradle is used to hopefully automate away linting and publishing to the loki server. Note: If you are a mac/linux user, you may need to run the following before you're OS allows you to run `./gradlew` commands: `chmod +x gradlew`.

#### For publishing all files in folder cist-1300:
 - Linux/Mac (Terminal): `./gradlew publish`
 - Windows (Command Prompt/Powershell): `.\gradlew publish`
 - Both: follow prompts for username and password input when running
 
#### For linting all files in folder cist-1300:
 - Linux/Mac (Terminal): `./gradlew lintHtml`
 - Windows (Command Prompt/Powershell): `.\gradlew lintHtml`
 - Note: First time this command is run, there won't be a lot printed for a bit. (This was done to avoid cluttering your console)
   - If you want to see the messages you're missing: Open the gradle.properties file and add a '#' symbol to the beginning of the line 'org.gradle.console = plain' before you run this command for the first time.


NOTE: Gradle requires Java to be installed on your machine (Windows or Linux/Mac)
 
### Installing Java
I can happily help anyone in the slack channel setup their java installation after they have at least watched their OS-related video:
 - Windows (10): https://www.youtube.com/watch?v=Wp6uS7CmivE&vl=en
 - Mac: https://www.youtube.com/watch?v=y6szNJ4rMZ0
 - Linux: Just ping me because there aren't any good videos that I would recommend.